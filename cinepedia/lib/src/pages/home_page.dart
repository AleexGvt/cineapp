import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(66, 67, 69, 1),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(40, 42, 46, 1),
          elevation: 1.0,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Cinepedia'),
              Container(
                padding: EdgeInsets.all(10),
                  child: Image(
                    image: AssetImage('logo.png'),
                    width: 50.0,
                    height: 50.0,
                ),
              ),
            ],
          ),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.search),
              onPressed: (){},
            ),

          ],
         ),
          drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              height: 140.0,
              child: DrawerHeader(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('', textAlign: TextAlign.left, style: TextStyle(fontSize: 20),),
                    Text('', style: TextStyle(color: Colors.grey, fontSize: 14),),
                  ],
                ),
              ),
            ),
            Divider(thickness: 1.0,),
            ListTile(
              leading: Icon(Icons.chrome_reader_mode),
              title: Text('Comprobante'),
              onTap: () {
                Navigator.pushNamed(context, 'comprobante');
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Cerrar Sesión'),
              onTap: () {
                Navigator.pushNamed(context, '/');
              },
            )
          ],
        ),
      ),

      body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: SingleChildScrollView( 
                         
      child: Center(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.9,
              child: Carousel(
              boxFit: BoxFit.cover,
                  autoplay: true,
                  animationCurve: Curves.fastOutSlowIn,
                  animationDuration: Duration(milliseconds: 1000),
                  dotSize: 6.0,
                  dotIncreasedColor: Color(0xFFFF335C),
                  dotBgColor: Colors.transparent,
                  dotPosition: DotPosition.topRight,
                  dotVerticalPadding: 10.0,
                  showIndicator: true,
                  indicatorBgPadding: 7.0,
                  images: [
                    Image(image:NetworkImage('https://i.pinimg.com/originals/4b/69/3c/4b693cc259bb72cfccc17a73f537ae81.png'), fit: BoxFit.fill, semanticLabel: 'Hola we',),
                    Image(image:NetworkImage('https://images8.alphacoders.com/457/457955.jpg'), fit: BoxFit.fill,),
                    ],             
                  ),

  
          ), 

            SizedBox(height: 30,),
            Image(image: AssetImage('cartelera.png'),height: 50,),        
            SizedBox(height: 30,),
            _crearRow(),
            _crearRow(),
            _crearRow(),
            
            /*Card(
                child: Container(
                  //padding: ,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          
                        ),
                    ],
                  ),
                  
                ),
              ),        
              */

        

           ],
          ),
     ), 
    ),
   ),
   );
  
 
  }

  Widget _crearRow(){
    return Row(
      children: <Widget>[       
         Expanded(
              child: Card(
                color: Color.fromRGBO(172, 172, 173, 1),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                         ListTile(
                          leading: Image(image:NetworkImage('https://i.pinimg.com/originals/4b/69/3c/4b693cc259bb72cfccc17a73f537ae81.png')),
                          title: Text('Spiderman'),
                      ), 
                      ButtonBar(
                        children: <Widget>[
                          FlatButton(
                            child: Text("Comprar boletos",style: TextStyle(color: Color.fromRGBO(245, 241, 5, 1)),),
                            onPressed: (){/** */},
                            ),
                          FlatButton(
                            child: Text("Ver reseña",style: TextStyle(color: Color.fromRGBO(245, 241, 5, 1)),),
                           onPressed: (){
                              Navigator.pushNamed(context, 'resena');
                            },
                            ),
                        ],
                      ),
                    ],
                  ),
                ),
            ),    

              Expanded(
              child: Card(
                color: Color.fromRGBO(172, 172, 173, 1),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                         ListTile(
                          leading: Image(image:NetworkImage('https://i.pinimg.com/originals/4b/69/3c/4b693cc259bb72cfccc17a73f537ae81.png')),
                          title: Text('Spiderman'),
                      ),
                       ButtonBar(
                        children: <Widget>[
                          FlatButton(
                            child: Text("Comprar boletos",style: TextStyle(color: Color.fromRGBO(245, 241, 5, 1)),),
                            onPressed: (){/** */},
                            ),
                          FlatButton(
                            child: Text("Ver reseña",style: TextStyle(color: Color.fromRGBO(245, 241, 5, 1)),),
                            onPressed: (){
                              Navigator.pushNamed(context, 'resena');
                            },
                            ),
                        ],
                      ),
                    ],
                  ),
                ),
            ),

            Expanded(
              child: Card(
                color: Color.fromRGBO(172, 172, 173, 1),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                         ListTile(
                          leading: Image(image:NetworkImage('https://i.pinimg.com/originals/4b/69/3c/4b693cc259bb72cfccc17a73f537ae81.png')),
                          title: Text('Spiderman'),
                      ), 
                      ButtonBar(
                        children: <Widget>[
                          FlatButton(
                            child: Text("Comprar boletos",style: TextStyle(color: Color.fromRGBO(245, 241, 5, 1)),),
                            onPressed: (){/** */},
                            ),
                          FlatButton(
                            child: Text("Ver reseña",style: TextStyle(color: Color.fromRGBO(245, 241, 5, 1)),),
                            onPressed: (){
                              Navigator.pushNamed(context, 'resena');
                            },
                            ),
                        ],
                      ),
                    ],
                  ),
                ),
            ),    
      ],
    
    );
  }


}