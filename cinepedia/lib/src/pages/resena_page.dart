import 'package:flutter/material.dart';
import 'package:youtube_player/youtube_player.dart';


class ResenaPage extends StatefulWidget {
  ResenaPage({Key key}) : super(key: key);

  @override
  _ResenaPageState createState() => _ResenaPageState();
}

class _ResenaPageState extends State<ResenaPage> {
  @override

  VideoPlayerController _videoController;
  bool isMute = false;

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(66, 67, 69, 1),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(40, 42, 46, 1),
          elevation: 1.0,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Cinepedia'),
              Container(
                padding: EdgeInsets.all(10),
                  child: Image(
                    image: AssetImage('logo.png'),
                    width: 50.0,
                    height: 50.0,
                ),
              ),
            ],
          ),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.search),
              onPressed: (){},
            ),

          ],
         ),
          drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              height: 140.0,
              child: DrawerHeader(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('', textAlign: TextAlign.left, style: TextStyle(fontSize: 20),),
                    Text('', style: TextStyle(color: Colors.grey, fontSize: 14),),
                  ],
                ),
              ),
            ),
            Divider(thickness: 1.0,),
            ListTile(
              leading: Icon(Icons.chrome_reader_mode),
              title: Text('Comprobante'),
              onTap: () {
                Navigator.pushNamed(context, 'comprobante');
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Cerrar Sesión'),
              onTap: () {
                Navigator.pushNamed(context, '/');
              },
            )
          ],
        ),
      ),

      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          child: Column( 
            children: <Widget>[
               YoutubePlayer(
              context: context,
              source: "https://www.youtube.com/watch?v=OIg_OqnGE0g",
              quality: YoutubeQuality.HD,
              aspectRatio: 16 / 9,
              autoPlay: true,
              loop: false,
              reactToOrientationChange: true,
              startFullScreen: false,
              controlsActiveBackgroundOverlay: true,
              controlsTimeOut: Duration(seconds: 4),
              playerMode: YoutubePlayerMode.DEFAULT,
              callbackController: (controller) {
                _videoController = controller;
              },
              onError: (error) {
                print(error);
              },
            ),
            SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.play_arrow),
                        onPressed: () => _videoController.value.isPlaying
                            ? null
                            : _videoController.play(),
                      ),
                      IconButton(
                        icon: Icon(Icons.pause),
                        onPressed: () => _videoController.pause(),
                      ),
                      IconButton(
                        icon: Icon(isMute ? Icons.volume_off : Icons.volume_up),
                        onPressed: () {
                          _videoController.setVolume(isMute ? 1 : 0);
                          setState(
                            () {
                              isMute = !isMute;
                            },
                          );
                        },
                      ),
                    ],
                  ),
          ],
        ),
      ),
    );
  }
}