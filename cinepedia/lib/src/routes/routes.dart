import 'package:cinepedia/src/pages/resena_page.dart';
import 'package:flutter/material.dart';
import 'package:cinepedia/src/pages/home_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes(){
  return <String, WidgetBuilder>{

  '/'               : (BuildContext context) => HomePage(),
  'resena'          : (BuildContext context) => ResenaPage(), 
  
  };
}